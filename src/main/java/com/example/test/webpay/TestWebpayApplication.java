package com.example.test.webpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestWebpayApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestWebpayApplication.class, args);
	}

}
