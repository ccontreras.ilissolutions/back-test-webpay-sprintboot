# README - Integración Webpay Plus con Spring Boot

Este repositorio contiene un proyecto que implementa la integración con WebPay Plus, un servicio de pago en línea ampliamente utilizado en chile. El proyecto proporciona un controlador con cinco endpoints que facilitan diversas operaciones relacionadas con transacciones financieras.


# Requisitos de Configuración

Antes de utilizar este proyecto, es necesario crear en tu archivo `/resources/application.properties` las siguientes variables de Entorno:

```
transbank.commerceCode=597055555532
transbank.apiKey=579B532A7440BB0C9079DED94D31EA1615BACEB56610332264630D42D0A36B1C
```

> [!IMPORTANTE]
> Estas variables son solo de prueba especificamente para webpay plus, consultar la documentación de transbank por si llegan a cambiar...  [Documentación Oficial Transbank : Llaves](https://www.transbankdevelopers.cl/documentacion/como_empezar#codigos-de-comercio), cuando pases a producción se reemplazarian dichas variables por las que te entrege el proveedor.

Adicionalmente se informa que para este ejemplo se utilizaron los siguientes ambientes:

- [ ] Java 21
- [ ] SpringBoot 3.2.4 + Plugins
- [ ] Maven 4.0
- [ ] SDK de Webpay para Java [Documentación Oficial Transbank : SDK Java](https://www.transbankdevelopers.cl/documentacion/como_empezar#proceso-tecnico-de-integracion).
 

# Endpoints del Controlador

## 1. Crear Transacción

### Endpoint: `/webpay`
**Método:** POST

**Pre-Requisitos:** enviar en el body un objeto JSON con la siguiente estructura:
```
{
  "buyOrder": String,
  "sessionId": String,
  "amount": double,
  "returnUrl": String
}
```
**Descripción:** Este endpoint envía una solicitud con el objeto indicado y recibe como respuesta otro objeto con una URL y un token, la url y el token son utilizados desde el front enviandolo como un formulario para iniciar la transacción.

para mas detalles: [Documentación Oficial Transbank : Crear Transacción](https://www.transbankdevelopers.cl/referencia/webpay#crear-una-transaccion).


## 2. Confirmar Transacción

### Endpoint: `/webpay/commit`
**Método:** POST

**Pre-Requisitos:** Enviar como parametro de la URL  el token

**Descripción:** Este endpoint envía una solicitud lo anteriormente indicado y recibe como respuesta otro objeto con una serie de datos que confirman el proceso de la transacción.

> [!IMPORTANTE]
> Este endpoint no aplica para transacciones que no se hayan procesado o que el usuario haya decidio anular manualmente.

para mas detalles: [Documentación Oficial Transbank : Confirmar Transacción](https://www.transbankdevelopers.cl/referencia/webpay#confirmar-una-transaccion).

## 3. Consultar Transacción

### Endpoint: `/webpay/search`
**Método:** POST

**Pre-Requisitos:** Enviar como parametro de la URL  el token

**Descripción:** Este endpoint envía una solicitud lo anteriormente indicado y recibe como respuesta otro objeto con una serie de datos que confirman el proceso de la transacción, mientras tenga una vigencia de 7 días o menos.

> [!TIP]
> Los token no procesados se guardan con status INITIALIZED.

para mas detalles: [Documentación Oficial Transbank : Consultar Transacción](https://www.transbankdevelopers.cl/referencia/webpay#obtener-estado-de-una-transaccion).

## 4. Anular Transacción

### Endpoint: `/webpay/refund`
**Método:** POST

**Pre-Requisitos:** Enviar como parametro de la URL  el token y en el body un objeto JSON con la siguiente estructura:
```
{
  "amount": double
}
```
**Descripción:** Este endpoint envía una solicitud lo anteriormente indicado y recibe como respuesta otro objeto con una serie de datos que confirman que la transacción fue reversada.

para mas detalles: [Documentación Oficial Transbank : Anular Transacción](https://www.transbankdevelopers.cl/referencia/webpay#reversar-o-anular-un-pago).

## 5. Capturar Transacción

### Endpoint: `/webpay/capture`
**Método:** POST

**Pre-Requisitos:** Enviar como parametro de la URL  el token y en el body un objeto JSON con la siguiente estructura:
```
{
    "buyOrder": String,
    "authorizationCode": String,
    "amount": double
}
```
**Descripción:** Este endpoint envía una solicitud con el objeto indicado y recibe como respuesta otro objeto con una URL y un token, la url y el token son utilizados desde el front enviandolo como un formulario para capturar la transacción.

> [!NOTA]
> Este endpoint da problemas notificando que el negocio no esta autorizado para realizar la operación, esta pendiente revisar si es tema de la integración o del código.

para mas detalles: [Documentación Oficial Transbank : Capturar Transacción](https://www.transbankdevelopers.cl/referencia/webpay#capturar-una-transaccion).


# Instalación y Usos

1. Clonar este repositorio en su entorno local.
  ```
  cd existing_repo
  git remote add origin https://gitlab.com/ccontreras.ilissolutions/back-test-webpay-sprintboot.git
  git branch -M main
  git push -uf origin main
  ```
2. Configurar el entornos según los requisitos mencionados anteriormente.
3. Instalar las dependencias del proyecto usando el gestor de paquetes de su elección.
4. Ejecutar la aplicación.
5. Puede enviar solicitudes a los endpoints que se mencionaran mas adelante utilizando un cliente HTTP.


# Contribución

Siéntase libre de enviar solicitudes de extracción con mejoras, correcciones de errores o nuevas características. Las contribuciones son bienvenidas.


# Problemas

Si encuentra algún problema o tiene alguna pregunta, no dude en abrir un problema en este repositorio.

¡Gracias por su interés en este proyecto!


> [!NOTA]
>  Este proyecto es solo con fines educativos y de demostración. Se recomienda no usar información sensible o datos de producción mientras se prueba..
